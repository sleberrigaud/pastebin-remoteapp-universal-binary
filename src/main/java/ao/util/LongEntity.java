package ao.util;

import net.java.ao.RawEntity;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;

public interface LongEntity extends RawEntity<Long>
{
    @AutoIncrement
    @NotNull
    @PrimaryKey("ID")
    public long getId();
}
