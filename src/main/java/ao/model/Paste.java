package ao.model;

import ao.util.LongEntity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Unique;

public interface Paste extends LongEntity
{
    @NotNull
    @Unique
    String getHash();

    void setHash(String hash);

    @NotNull
    String getKey();

    void setKey(String key);
}
