package services.dao;

import ao.model.Paste;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.labs.remoteapps.api.annotation.ServiceReference;
import com.google.common.collect.ImmutableMap;
import net.java.ao.Query;
import org.springframework.stereotype.Component;

import static com.google.common.base.Preconditions.*;

@Component
public final class PasteDao
{
    private final ActiveObjects ao;

    public PasteDao(@ServiceReference ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    public Paste findByHash(String hash)
    {
        final Paste[] pastes = ao.find(Paste.class, Query.select().where("HASH = ?", hash));
        if (pastes.length == 0)
        {
            return null;
        }
        else if (pastes.length > 1)
        {
            throw new IllegalStateException("Found multiple pastes for hash '" + hash + "'");
        }
        else
        {
            return pastes[0];
        }
    }

    public Paste create(String sha1, String key)
    {
        return ao.create(Paste.class, ImmutableMap.<String, Object>of("HASH", sha1, "KEY", key));
    }
}
