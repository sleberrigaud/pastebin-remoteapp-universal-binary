package services.velocity;

import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.log.CommonsLogLogChute;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.Map;

/**
 * A velocity template renderer
 */
@Component
public final class VelocityTemplateRenderer implements TemplateRenderer
{
    private final VelocityEngine velocity;

    public VelocityTemplateRenderer()
    {
        velocity = new VelocityEngine();
        overrideProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS, CommonsLogLogChute.class.getName());
        overrideProperty(Velocity.RESOURCE_LOADER, "classpath");
        overrideProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        overrideProperty("classpath.resource.loader.cache", Boolean.toString(!Boolean.getBoolean("atlassian.dev.mode")));

        velocity.init();
    }

    public void render(String templateName, Writer writer) throws RenderingException, IOException
    {
        render(templateName, Collections.<String, Object>emptyMap(), writer);
    }

    public void render(String templateName, Map<String, Object> context, Writer writer)
            throws RenderingException, IOException
    {
        try
        {
            Template template = velocity.getTemplate(templateName);
            template.merge(createContext(context), writer);
            writer.flush();
        }
        catch (IOException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RenderingException(e);
        }
    }

    public String renderFragment(String fragment, Map<String, Object> context)
    {
        try
        {
            StringWriter tempWriter = new StringWriter(fragment.length());
            velocity.evaluate(createContext(context), tempWriter, "renderFragment", fragment);
            return tempWriter.toString();
        }
        catch (Exception e)
        {
            throw new RenderingException(e);
        }
    }

    private VelocityContext createContext(Map<String, Object> contextParams)
    {
        return new VelocityContext(contextParams);
    }


    private void overrideProperty(String key, Object value)
    {
        // "userdirective" property can have multiple values; all other overrideable
        // properties that we set at init time should have single values
        if (key.equals("userdirective"))
        {
            velocity.addProperty(key, value);
        }
        else
        {
            velocity.setProperty(key, value);
        }
    }

    /**
     * Check whether the given template exists or not
     */
    public boolean resolve(String templateName)
    {
        return this.getClass().getResource(templateName) != null;
    }
}
