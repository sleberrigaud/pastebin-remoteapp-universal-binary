package servlets;

import ao.model.Paste;
import com.atlassian.fugue.Option;
import com.atlassian.labs.remoteapps.kit.servlet.AppUrl;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.lang.StringUtils;
import org.leberrigaud.pastebin.utils.Api;
import org.leberrigaud.pastebin.utils.Hash;
import org.leberrigaud.pastebin.utils.Languages;
import org.leberrigaud.pastebin.utils.PastebinUrl;
import org.springframework.stereotype.Component;
import services.dao.PasteDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import static com.google.common.base.Preconditions.*;

@AppUrl("/pastebin")
@Component
public final class MacroServlet extends AbstractPastebinServlet
{
    private final TemplateRenderer templateRenderer;
    private final PasteDao pasteDao;

    public MacroServlet(TemplateRenderer templateRenderer, PasteDao pasteDao)
    {
        this.templateRenderer = checkNotNull(templateRenderer);
        this.pasteDao = checkNotNull(pasteDao);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        final String id = req.getParameter("id");
        final String body = req.getParameter("body");
        final String lang = req.getParameter("lang");

        macro(id, body, lang, resp);
    }

    private void macro(String id, String body, String lang, HttpServletResponse resp) throws IOException
    {
        if (StringUtils.isBlank(id) && StringUtils.isNotBlank(body))
        {
            final String actualLang = Languages.forRequestParameter(lang);
            final String sha1 = Hash.of(body, actualLang);

            final Paste paste = pasteDao.findByHash(sha1);
            if (paste != null)
            {
                ok(paste.getKey(), resp.getWriter());
                return;
            }

            final String url = Api.post(
                    body,
                    actualLang,
                    new Api.Call<String>()
                    {
                        public String call(String url, ImmutableMap<String, String> parameters)
                        {
                            final String response =
                                    Client.create(new DefaultClientConfig())
                                            .resource(url)
                                            .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                                            .post(String.class, toFormParameters(parameters));

                            log.debug("This is the response to the PasteBin creation: " + response);

                            final Paste p = pasteDao.create(sha1, PastebinUrl.id(response));
                            return p.getKey();
                        }

                        private MultivaluedMap toFormParameters(ImmutableMap<String, String> parameters)
                        {
                            MultivaluedMap formData = new MultivaluedMapImpl();
                            for (Map.Entry<String, String> p : parameters.entrySet())
                            {
                                formData.add(p.getKey(), p.getValue());
                            }
                            return formData;
                        }
                    },
                    Suppliers.ofInstance(Option.<String>none()));

            ok(PastebinUrl.id(url), resp.getWriter());
        }
        else if (StringUtils.isNotBlank(id))
        {
            ok(PastebinUrl.id(id), resp.getWriter());
        }
        else
        {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Macro does NOT define a body NOR a URL");
        }
    }

    private void ok(String id, Writer writer) throws IOException
    {
        templateRenderer.render("templates/pastebin.vm", ImmutableMap.<String, Object>of("id", id), writer);
    }
}
