package servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;

abstract class AbstractPastebinServlet extends HttpServlet
{
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
}
