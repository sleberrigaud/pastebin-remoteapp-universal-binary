package servlets;

import com.atlassian.labs.remoteapps.kit.servlet.AppUrl;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AppUrl("/remote-app-started")
@Component
public final class RemoteAppStartedServlet extends AbstractPastebinServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        log.debug("Posted! some app is registering itself!");
    }
}
